# Alfred Placeholder Images

## Description
A workflow which will generate links from placeholder images of any size you want.

## Dependencies
* PHP

## Examples
* phi 20,50,jpg,Hello
  * Result: 20x50 JPG Image with "Hello" as text
* phi 500
  * Result: 500x500 JPG Image
* phi 500,800
  * Result: 500x800 JPG Image
* phi 500,png
  * 500x500 PNG Image
* phi 500,png,Just a small test
  * 500x500 PNG Image with "Just a small test" as Text